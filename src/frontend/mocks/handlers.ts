import { listOrganizations } from '../js/api/mocks/joanie/organizations';
import { listCourses, getCourse } from '../js/api/mocks/joanie/courses';

export const handlers = [listOrganizations, listCourses, getCourse];
